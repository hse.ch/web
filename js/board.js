var apiUrl = '/api';

function onError(xhr) {
	alert(msg.REQUEST_FAILED);
}

/* Collection of presenter functions that render resource content */

var display = {
	boards: function (boards) {
		var navbar = document.querySelector('header > nav > ul');
		var navsidebar = document.querySelector('aside > nav > ul');
		var selectBoard = document.querySelector('#create > select#board');
		navbar.innerText = '';
		navsidebar.innerText = '';
		boards.forEach(function(board) {
			makeChild(navbar, 'li', `<a href="${board.code}">${board.code}</a>`);
			makeChild(navsidebar, 'li', `<a href="${board.code}">${board.name}</a>`);
			var option = makeChild(selectBoard, 'option', `${board.code} – ${board.name}`);
			option.value = board.code;
		});
	},
	board: function (board) {
		setTitle(`${board.code} – ${board.name}`, board.code);
	},
	threads: function (threads) {
		var hidden = new Set(JSON.parse(window.localStorage.getItem('hidden') || '[]'));
		var threadsDiv = document.querySelector('div#threads');
		threadsDiv.innerText = '';
		threads.forEach(function(thread) {
			var section = makeChild(threadsDiv, 'section', `<h3><a href="/t/${thread.num}">${escapeHTML(thread.subj)}</a></h3>`);
			if (hidden.has(thread.num)) {
				section.classList.add('minimized');
			}
			section.classList.add('thread');
			section.id = `thread${thread.num}`;
			thread.posts[0].original = true;
			var postsDiv = makeChild(section, 'div', ``);
			postsDiv.classList.add('posts');
			thread.posts.forEach(function(post) {
				var details = `<ul class="details">
					<li><span><a href="/t/${thread.num}#post${post.num}">${('' + post.num).padStart(5, '0')}</a></span></li>
					<li class="reply"><a href="/reply/post${post.num}" class="material-icons">reply</a></li>
					<li class="goto"><a target="_blank" href="/t/${thread.num}" class="material-icons">forward</a></li>
					<li class="more"><span onclick="more(this);" class="material-icons"></span></li>
					<li class="hide"><span onclick="toggleHideThread(${thread.num});" class="extra material-icons"></span></li>
					<li class="report"><a href="/report/post${post.num}" class="extra material-icons">report</a></li>
					<li class="favor"><a href="/favor/thread${thread.num}" class="extra material-icons">star</a></li>
				</ul>`;
				var article = makeChild(postsDiv, 'article', `${details}<p>${escapeHTML(post.text)}</p>`);
				if (post.original) {
					article.classList.add('original');
				}
				article.classList.add('post');
				article.id = `post${post.num}`;
			});
		});
	},
	thread: function (thread) {
		var hidden = new Set(JSON.parse(window.localStorage.getItem('hidden') || '[]'));
		var threadsDiv = document.querySelector('div#threads');
		setTitle(`${thread.board.code} – ${thread.board.name}`, thread.board.code);
		threadsDiv.innerText = '';
		var section = makeChild(threadsDiv, 'section', `<h3><a href="/t/${thread.num}">${escapeHTML(thread.subj)}</a></h3>`);
		if (hidden.has(thread.num)) {
			section.classList.add('minimized');
		}
		section.classList.add('thread');
		section.id = `thread${thread.num}`;
		thread.posts[0].original = true;
		var postsDiv = makeChild(section, 'div', ``);
		postsDiv.classList.add('posts');
		thread.posts.forEach(function(post) {
				var details = `<ul class="details">
					<li><span><a href="/t/${thread.num}#post${post.num}">${('' + post.num).padStart(5, '0')}</a></span></li>
					<li class="reply"><a href="/reply/post${post.num}" class="material-icons">reply</a></li>
					<li class="goto"><a target="_blank" href="/t/${thread.num}" class="material-icons">forward</a></li>
					<li class="more"><span onclick="more(this);" class="material-icons"></span></li>
					<li class="hide"><span onclick="toggleHideThread(${thread.num});" class="extra material-icons"></span></li>
					<li class="report"><a href="/report/post${post.num}" class="extra material-icons">report</a></li>
					<li class="favor"><a href="/favor/thread${thread.num}" class="extra material-icons">star</a></li>
				</ul>`;
				var article = makeChild(postsDiv, 'article', `${details}<p>${escapeHTML(post.text)}</p>`);
				if (post.original) {
					article.classList.add('original');
				}
				article.classList.add('post');
				article.id = `post${post.num}`;
			});
	},
	posts: function (posts) {
		posts[0].original = true;
		var threadSection = document.querySelector(`section.thread`);
		var postsDiv = makeChild(threadSection, 'div', ``);
		postsDiv.classList.add('posts');
		posts.forEach(function(post) {
			var details = `<ul class="details">
				<li><span><a href="/t/${post.thread.num}#post${post.num}">${('' + post.num).padStart(5, '0')}</a></span></li>
				<li class="reply"><a href="/reply/post${post.num}" class="material-icons">reply</a></li>
				<li class="goto"><a target="_blank" href="/t/${post.thread.num}" class="material-icons">forward</a></li>
				<li class="more"><span onclick="more(this);" class="material-icons"></span></li>
				<li class="hide"><span onclick="toggleHideThread(${post.thread.num});" class="extra material-icons"></span></li>
				<li class="report"><a href="/report/post${post.num}" class="extra material-icons">report</a></li>
				<li class="favor"><a href="/favor/thread${post.thread.num}" class="extra material-icons">star</a></li>
			</ul>`;
			var article = makeChild(postsDiv, 'article', `${details}<p>${escapeHTML(post.text)}</p>`);
			if (post.original) {
				article.classList.add('original');
			}
			article.classList.add('post');
			article.id = `post${post.num}`;
		});
	},
	search: function (posts) {
		var threadsDiv = document.querySelector('div#threads');
		threadsDiv.innerText = '';
		var section = makeChild(threadsDiv, 'section', `<h3>${msg.SEARCH_RESULTS}</h3><div class="posts"></div>`);
		section.classList.add('thread');
		var postsDiv = document.querySelector('div.posts');
		posts.forEach(function(post) {
			var details = `<ul class="details">
				<li><span><a href="/t/${post.thread.num}#post${post.num}">${('' + post.num).padStart(5, '0')}</a></span></li>
				<li class="reply"><a href="/reply/post${post.num}" class="material-icons">reply</a></li>
				<li class="goto"><a target="_blank" href="/t/${post.thread.num}" class="material-icons">forward</a></li>
				<li class="more"><span onclick="more(this);" class="material-icons"></span></li>
				<li class="hide"><span onclick="toggleHideThread(${post.thread.num});" class="extra material-icons"></span></li>
				<li class="report"><a href="/report/post${post.num}" class="extra material-icons">report</a></li>
				<li class="favor"><a href="/favor/thread${post.thread.num}" class="extra material-icons">star</a></li>
			</ul>`;
			var article = makeChild(postsDiv, 'article', `${details}<p>${escapeHTML(post.text)}</p>`);
			article.classList.add('post');
			article.id = `post${post.num}`;
		});
	}
}

/* Function that fetches resource and calls its relevant presenter */

function update(resource, parameters={}, postHook=function() {}) {
	var searchParams = new URLSearchParams(parameters).toString();
	sendXhr('GET', `${apiUrl}/${resource}?${searchParams}`, ``,
		function (xhr) {
			if (xhr.status <= 200 && xhr.status < 300) {
				var content = xhr.response;
				display[resource](content);
				postHook();
			}
			else {
				setTitle(msg.NOTFOUND_TITLE);
			}
		}, onError
	);
}

function submit(resource, parameters={}) {
	var submitButton = document.querySelector('#create > #submit');
	if (submitButton.innerText == 'hourglass_top') {
		return;
	}
	submitButton.innerText = 'hourglass_top';
	var formData = new URLSearchParams(parameters).toString();
	sendXhr('POST', `${apiUrl}/${resource}`, formData,
		function (xhr) {
			if (xhr.status <= 200 && xhr.status < 300) {
				var content = xhr.response;
				document.querySelector('#create').classList.add('hidden');
				document.querySelector('#create > select#board').value = '';
				document.querySelector('#create > input#subj').value = '';
				document.querySelector('#create > textarea#text').value = '';
				submitButton.innerText = 'send';
				updateContent();
			}
			else {
				submitButton.innerText = 'cancel_schedule_send';
			}
		}, onError
	);
}

/* Functions that update the creation section */

function displayCreate(title, subject=false, board=false) {
	var h3 = document.querySelector('#create > h3');
	var subj = document.querySelector('#create > input#subj');
	var brd = document.querySelector('#create > select#board');
	h3.innerText = title;
	subj.toggleAttribute('disabled', !subject);
	brd.toggleAttribute('disabled', !board);

}

function updateCreate(type, parameters={}) {
	var submit = document.querySelector('#create > #submit');
	submit.onclick = function() {
		submitContent(type, parameters);
	};
	switch (type) {
		case 'thread':
			displayCreate(msg.CREATE_THREAD, true, true);
			break;
		case 'board-thread':
			displayCreate(msg.CREATE_THREAD + ' на ' + parameters.code, true);
			break;
		case 'thread-post':
			displayCreate(msg.CREATE_POST + ' в ' + parameters.num + '-м треде');
			break;
	}
}

/* Function that updates content relevant to the resource */

function updateContent() {
	update('boards', {}, function() {
		var path = splitPath();
		if (path.length > 2 && path[2]) {
			setTitle(msg.NOTFOUND_TITLE);
		}
		if (path.length > 1 && path[1] && path[0] == 't') {
			var num = path[1];
			updateCreate('thread-post', {num: num});
			update('thread', {num: num});
		}
		else if (path.length > 0 && path[0]) {
			var code = `/${path[0]}/`;
			updateCreate('board-thread', {code: code});
			update('board', {code: code}, function() {
				update('threads', {code: code});
			});
		}
		else {
			setTitle(msg.HOMEPAGE_TITLE);
			updateCreate('thread');
			update('threads');
		}
	});
}

/* Function that submits content creation */

function submitContent(type, parameters={}) {
	switch (type) {
		case 'thread':
			parameters.code = document.querySelector('#create > select#board').value;
		case 'board-thread':
			parameters.subj = document.querySelector('#create > input#subj').value;
			parameters.text = document.querySelector('#create > textarea#text').value;
			submit('thread', parameters);
			break;
		case 'thread-post':
			parameters.text = document.querySelector('#create > textarea#text').value;
			submit('post', parameters);
			break;
	}
}

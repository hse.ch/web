/* Functions for theme picker */

function onThemeChange() {
	var themePicker = document.querySelector('#theme');
	setTheme(themePicker.value);
}

function setTheme(themeId) {
	var themeSheet = document.querySelector('#themesheet');
	themeId = themeId || window.localStorage.getItem('theme') || thm[0].code;
	window.localStorage.setItem('theme', themeId);
	themeSheet.href = '/css/' + themeId + '.css';
}

function updateThemePicker() {
	var themePicker = document.querySelector('#theme');
	var themeId = window.localStorage.getItem('theme');
	themePicker.value = themeId;
}

function initThemePicker() {
	var themePicker = document.querySelector('select#theme');
	themePicker.innerText = '';
	thm.forEach(function(theme) {
		var option = makeChild(themePicker, 'option', theme.name);
		option.value = theme.code;
	})
	themePicker.onchange = onThemeChange;
	setTheme();
	updateThemePicker();
}

/* Functions for sidebar toggler chevron */

function toggleSidebar() {
	setSidebar(true);
}

function setSidebar(change=false) {
	var sidebarChevron = document.querySelector('#sidebar-chevron > .material-icons');
	var sidebar = document.querySelector('aside');
	var state = (window.localStorage.getItem('sidebar') || 'on') != 'off';
	state = change ? !state : state;
	window.localStorage.setItem('sidebar', state ? 'on' : 'off');
	sidebarChevron.classList.toggle('expanded', state);
	sidebar.classList.toggle('hidden', !state);
}

function initSidebarChevron() {
	setSidebar();
}

/* Function that hide / show threads */

function toggleHideThread(num) {
	var thread = document.querySelector(`section#thread${num}`);
	var hidden = new Set(JSON.parse(window.localStorage.getItem('hidden') || '[]'));
	if (thread.classList.toggle('minimized', !hidden.has(num))) {
		hidden.add(num);
	}
	else {
		hidden.delete(num);
	}
	window.localStorage.setItem('hidden', JSON.stringify([...hidden]));
}

/* Initialize UI components */

function initUi() {
	initThemePicker();
	initSidebarChevron();
}

/* Function for expanding and collapsing lists */

function more(node) {
	node.parentNode.classList.toggle('expanded');
}

/* Function with callbacks for making a JSON XMLHttpRequest */

function sendXhr(method, url, body, onLoad, onError) {
	var xhr = new XMLHttpRequest();
	if (onLoad != undefined) {
		xhr.addEventListener('load', () => onLoad(xhr));
	}
	if (onError != undefined) {
		xhr.addEventListener('error', () => onError(xhr));
	}
	if (typeof body != 'string') {
		body = JSON.stringify(body);
	}
	xhr.responseType = 'json';
	xhr.open(method, url);
	if (method == 'POST' && body) {
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	}
	xhr.send(body);
}

/* Function to escape HTML special characters */

function escapeHTML(unsafe) {
	return unsafe
		.replace(/&/g, '&amp;')
		.replace(/</g, '&lt;')
		.replace(/>/g, '&gt;')
		.replace(/"/g, '&quot;')
		.replace(/'/g, '&#039;');
}

/* Function to create, attach and return a child DOM node */

function makeChild(parent, tag, content) {
	var node = document.createElement(tag)
	node.innerHTML = content;
	parent.appendChild(node);
	return node;
}

/* Function to split location pathname */

function splitPath(path) {
	var path = window.location.pathname.substr(1);
	return path.split('/');
}

/* Function to set document heading and title */

function setTitle(titleText, link) {
	var h2 = document.querySelector('h2');
	if (link != undefined) {
		h2.innerHTML = `<a href='${link}'>${escapeHTML(titleText)}</a>`;
	}
	else {
		h2.innerText = titleText;
	}
	document.title = `${titleText} · ${msg.SITE_NAME}`;
}

/* Function to toggle creation section */

function createContent() {
	var create = document.querySelector('div#create');
	create.classList.toggle('hidden');
}

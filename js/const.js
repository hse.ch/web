var msg = {
	SITE_NAME: 'Вышка.ч',
	CREATE_THREAD: 'Создать тред',
	CREATE_POST: 'Создать пост',
	SEARCH_RESULTS: 'Результаты поиска',
	HOMEPAGE_TITLE: 'Non Scholae, Sed Vitae Discimus',
	BAD_REQUEST: '400 – Вышка.ч осуждает ваш запрос.',
	NOTFOUND_TITLE: '404 – Вышка.ч не владеет такой информацией.',
	REQUEST_FAILED: 'Не удалось совершить запрос. Обновите страницу и попробуйте еще раз.'
}

var thm = [
	{code: 'classicblue', name: 'Classic Blue'},
	{code: 'brightfuture', name: 'Bright Future'},
	{code: 'notsleeping', name: 'Not Sleeping'}
]
